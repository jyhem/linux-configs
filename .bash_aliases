# Jyhem's bash customisation
export LC_COLLATE=C
export PATH=$PATH:$HOME/bin

alias ll='ls -laF'
alias lrt='ls -laFrt'
alias jj='jobs -l'
alias xx=exit
alias xxx=exit
alias kk=exit
alias sss='. ~/.bashrc'
alias m=more
alias h=history
alias spy='ps auwx | grep '
alias a=alias
alias vi=vim
alias jml_gitlog='echo "git log --graph --abbrev-commit --oneline --all"; git log --graph --abbrev-commit --oneline --all'
alias l='ls -CF'
alias ff='firefox -no-remote -ProfileManager'
alias hh='chromium-browser --user-data-dir=etc/chromium-profiles/HH &'
alias dff='df -x tmpfs -x devtmpfs -x squashfs'
alias dig='echo "dig +noall +answer";dig +noall +answer'
alias tmux_wake='sudo killall -SIGUSR1 tmux'

# GIT shortcuts
alias vih='sudo runuser -u apache -- vim '
alias gg='sudo runuser -u apache --'
alias ggc='git checkout'
alias ggs='git status'
alias ggd='git diff'
alias gga='git add'
alias ggb='git branch'
alias ggco='git commit'
alias gggc='sudo runuser -u apache -- git checkout'
alias gggs='sudo runuser -u apache -- git status'
alias gggd='sudo runuser -u apache -- git diff'
alias ggga='sudo runuser -u apache -- git add'
alias gggb='sudo runuser -u apache -- git branch'
alias gggco='sudo runuser -u apache -- git commit'

# Tiki shortcuts
alias ppir="echo 'Executing: php console.php index:rebuild'; php console.php i:r | grep -v '^  indexed'"

# svn variations
alias greps='grep --exclude-dir=.svn'
alias svni='svn --ignore-externals'
alias svnl='svn --ignore-externals'
# Don't make internet lookups
alias vlcx='/usr/bin/vlc --no-auto-preparse --album-art 0 '
alias vlc='/usr/bin/vlc --no-auto-preparse --album-art 0 '

# If not export LC_COLLATE=C
#alias df='LC_ALL=C df'
#alias ll='LC_COLLATE=C ls -laF'

alias telegram='/opt/telegram/Telegram'
alias ak='akregator >/dev/null 2>&1'
[ -x /usr/bin/moste ] && export MANPAGER=most

# For RedHat style servers
## A more ubuntu-like prompt without [] which does not morph into links when pasted in a wiki page
export PS1='\[\e]0;\u@\h: \w\a\]\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;33m\]\w\[\033[00m\]\$ '
## Leave tmux tab label unchanged
export PROMPT_COMMAND=" "

# Like kubuntu default but yellow path
export PS1='\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;33m\]\w\[\033[00m\]\$ '
